<?php
/*
Plugin Name:  WooCommrece Peliculas
Plugin URI:   https://weblowcosts.com
Description:  Plugin donde se asocia una película a una orden
Version:      1
Author:       WordPress.org
Author URI:   https://developer.wordpress.org/
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  wporg
Domain Path:  /languages
*/


/**
 * Traida de archivos ecenciales
 * @since 1.0.0
 */
function bootstrap_js() {
	wp_enqueue_script( 'image_picker_js',
  					plugins_url('woocommrece-peliculas')  . '/src/image-picker.min.js', 
  					array(), 
  					'1'); 
	
	wp_enqueue_script( 'woocommrece_peliculas',
  					plugins_url('woocommrece-peliculas')  . '/src/woocommrece-peliculas.js', 
  					array(), 
  					'1', true); 
  					
	wp_enqueue_style( 'image_picker_css',
  					plugins_url('woocommrece-peliculas')  . '/src/image-picker.css', 
  					array(), 
  					'1'
  					);
  					
	wp_enqueue_style( 'woocommrece_peliculas_css',
  					plugins_url('woocommrece-peliculas')  . '/src/woocommrece-peliculas.css', 
  					array(), 
  					'1'
  					);  					
  					
}
add_action( 'wp_enqueue_scripts', 'bootstrap_js');



/**
 * Campo personalizado de genero de pelicula
 * @since 1.0.0
 */
function cfwc_create_custom_field() {
 $args = array(
 'id' => 'custom_field_genero_de_pelicual',
 'label' => __( 'Genero de pelicula', 'woocommrece-peliculas' ),
 'class' => 'select short',
 'options' =>  array( 'sin_categoria' => 'Sin Categoría', 'Comedia' => 'Comedia', 'Acción' => 'Acción', 'Aventura' => 'Aventura', 'Drama' => 'Drama'),
 'desc_tip' => false,
 'description' => __( 'Seleccione el género de la película con el que se relacione el producto.', 'woocommrece-peliculas' ),
 );
 woocommerce_wp_select( $args );
}
add_action( 'woocommerce_product_options_general_product_data', 'cfwc_create_custom_field' );





/**
 * Guardar campo personalizado de genero de pelicula
 * @since 1.0.0
 */
function cfwc_save_custom_field( $post_id ) {
    
    
    
    $product = wc_get_product( $post_id );
    $title = isset( $_POST['custom_field_genero_de_pelicual'] ) ? $_POST['custom_field_genero_de_pelicual'] : '';
    $product->update_meta_data( 'custom_field_genero_de_pelicual', sanitize_text_field( $title ) );
    $product->save();
}
add_action( 'woocommerce_process_product_meta', 'cfwc_save_custom_field' );






/**
 * Mostar campo en la vista
 * @since 1.0.0
 */
function cfwc_display_custom_field() {
 global $post;
 // Check for the custom field value
 $product = wc_get_product( $post->ID );
 $genero_de_pelicula = $product->get_meta( 'custom_field_genero_de_pelicual' );
 if( $genero_de_pelicula ) {
 // Only display our field if we've got a value for the field title
 
    if($genero_de_pelicula != 'sin_categoria'){
 
        if($genero_de_pelicula === 'Comedia'){
          $pelicula_id = 35; 
        }elseif($genero_de_pelicula === 'Acción'){
          $pelicula_id = 28;
        }elseif($genero_de_pelicula === 'Aventura'){
          $pelicula_id = 12; 
        }elseif($genero_de_pelicula === 'Drama'){
          $pelicula_id = 18;  
        }
     
     
        $ch = curl_init("https://api.themoviedb.org/3/discover/movie?api_key=e8b3f886a8849bce4d2586ac68d57d98&language=es&with_genres=" . $pelicula_id);  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        $result = curl_exec($ch);  
        curl_close($ch);  
        $peliculas_del_genero_php = json_decode($result, true);
    
        echo '<select class="select-peliculas" name="cfwc-title-field">';
        echo '<option data-img-src="https://pruebatecnica.weblowcosts.com/wp-content/uploads/2020/02/woocommerce-placeholder.png" value="sin_pelicula">Sin Película</option>';
        
        foreach( $peliculas_del_genero_php['results'] as $key_pelicula_del_genero => $pelicula_del_genero ){
            if($key_pelicula_del_genero < 10){
                echo '<option data-img-src="https://www.themoviedb.org/t/p/w600_and_h900_bestv2' . $pelicula_del_genero['backdrop_path'] . '" value="' . $pelicula_del_genero['original_title'] . '">' . $pelicula_del_genero['original_title'] . '</option>';
            }else{
                break;
            }
        }    
    
        echo '</select>';
        echo '<div class="left-bootom-pelicula bootom-pelicula"> < </div>';
        echo '<div class="right-bootom-pelicula bootom-pelicula"> > </div>';
    }else{
        echo "<input type='hidden' name='cfwc-title-field' value='sin_categoria' />";
    }
 }
}
add_action( 'woocommerce_before_add_to_cart_button', 'cfwc_display_custom_field' );





/**
 * Validar campo en la vista
 * @since 1.0.0
 * @param Array $passed Validation status.
 * @param Integer $product_id Product ID.
 * @param Boolean $quantity Quantity
 */
function cfwc_validate_custom_field( $passed, $product_id, $quantity ) {

    global $post;
    

    if( isset( $_POST['cfwc-title-field']) ) {
        
        if($_POST['cfwc-title-field'] === 'sin_pelicula'){
            
            // Fails validation
            $passed = false;
            wc_add_notice( __( 'Por favor, seleccione una película', 'woocommrece-peliculas' ), 'error' );
        
        }
        
    }
    return $passed;
}
add_filter( 'woocommerce_add_to_cart_validation', 'cfwc_validate_custom_field', 10, 3 );





/**
 * Añadir pelicula al carrito
 * @since 1.0.0
 * @param Array $cart_item_data Cart item meta data.
 * @param Integer $product_id Product ID.
 * @param Integer $variation_id Variation ID.
 * @param Boolean $quantity Quantity
 */
function cfwc_add_custom_field_item_data( $cart_item_data, $product_id, $variation_id, $quantity ) {
 if( ! empty( $_POST['cfwc-title-field'] ) ) {
 // Add the item data
 $cart_item_data['title_field'] = $_POST['cfwc-title-field'];
 }
 return $cart_item_data;
}
add_filter( 'woocommerce_add_cart_item_data', 'cfwc_add_custom_field_item_data', 10, 4 );






/**
 * Mostar la pelicula selecionada en el carrito y en checaut
 * @since 1.0.0
 */
function cfwc_cart_item_name( $name, $cart_item, $cart_item_key ) {
 if( isset( $cart_item['title_field'] ) ) {
     $name .= sprintf(
         ' (%s)',
         esc_html( $cart_item['title_field'] )
     );
 }

 return $name;
}
add_filter( 'woocommerce_cart_item_name', 'cfwc_cart_item_name', 10, 3 );






/**
 * Mostar pelicula en la orden y en el correo
 * @since 1.0.0
 */
function cfwc_add_custom_data_to_order( $item, $cart_item_key, $values, $order ) {
    foreach( $item as $cart_item_key=>$values ) {
        if( isset( $values['title_field'] ) ) {
            $item->add_meta_data( __( 'Película escogida', 'woocommrece-peliculas' ), $values['title_field'], true );
        }
    }
}
add_action( 'woocommerce_checkout_create_order_line_item', 'cfwc_add_custom_data_to_order', 10, 4 );





/**
 * Mostar pelicula en la orden y en el correo
 * @since 1.0.0
 */
add_filter( 'woocommerce_loop_add_to_cart_link', 'ts_replace_add_to_cart_button', 10, 2 );
function ts_replace_add_to_cart_button( $button, $product ) {
    

    
    $product = wc_get_product( $product->id );
    $genero_de_pelicula = $product->get_meta( 'custom_field_genero_de_pelicual' );    
            
    
    if( isset( $genero_de_pelicula ) ) {
        
        if($genero_de_pelicula != 'sin_categoria'){
            
            $text_bottom = __("VER PRODUCTO Y PELÍCULA", "woocommrece-peliculas");
            $button_link = $product->get_permalink(); // enlace directo del producto
            $button = '<a class="button product_type_simple add_to_cart_button" href="' . $button_link . '">' . $text_bottom . '</a>';
        
        }
        
    }    
    
    return $button;
}




